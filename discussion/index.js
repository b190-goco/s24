console.log("Hello World");

//SECTION - Exponent operator
// pre-es6
/*
	let/const variableName = Math.pow(number, exponent)
*/
let firstNum = Math.pow(8, 2);
console.log(firstNum);

// es6
/*
	let/const variableName = number ** exponent
*/
let secondNum = 8 ** 2;
console.log(secondNum);

// SECTION - Template Literals
/*
	- allows to write strings without using the concatination operator (+);
	- helps greatly in terms of code readability
*/

let name = "John";

// pre-es6
let message = "Hello " + name + "! Welcome to programming!";
// Message without template literals: message
console.log('Message without template literals: ' + message);

// es6
// single-line
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message without template literals: ${message}`);

// multiple-line
const anotherMessage = `
${name} won the math competition.
He won it by solving the problem 8 ** 2 with the solution of ${8 ** 2}.
`;
// He won it by solving the problem 8 ** 2 with the solution of ${secondNum}.

console.log(anotherMessage);
/*
	Miniactivity
		create 2 variables
			insterestRate which has the value of .15;
			principal which has the value of 1000

			log in the console the total interest (p * i) of the account that has the principal as its balance and the interestRate as its interest %. use template literals in doing this

			send the output in the console
*/
/*
	- Template literals allow us to write strings with embedded JS expressions
	- expressions in general are any valid unit of code that resolves into a value
	- "${}" are used to include JS expressions in strings using template literals
*/
// first solution (third variable)
let interestRate = .15;
let principal = 1000;
// let totalInterest = interestRate * principal;

// console.log(`The total interest on your savings account is: ${totalInterest}`);

// second solution (mathematical operation inside the template literals)
console.log(`The total interest on your savings account is: ${interestRate * principal}`);

// SECTION - Array Destructuring
/*
	- allows us to unpack elements in arrays to distinct variables
	- allows us to name array elements with variables instead of using index numbers
	- helps with code readability
	- SYNTAX:
			let/const [ variableA, variableB, variableC ] = arrayName
			NOTE: we have to be mindful of the variableName and make sure that it describes the right element that is stored inside it.
*/

const fullName = [ 'Juan', "Dela", "Cruz" ];
// pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// array destructuring
const [ firstName, middleName, lastName ] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
// using template literals
console.log(`Hello ${firstName} ${middleName} ${lastName}!`);

/*
	Miniactivity
		create a "person" object wiht the following properties
			firstName = Jane
			middleName = Dela
			lastName = Cruz
		log in the console each of the properties as well as a separate greetings message for the person
		send the output in the google chat
*/

// SECTION - Object Destructing
/*
	- allows us to unpack properties of objects into distinct variables
	-shortens the syntax for accessing the properties from an object
	SYNTAX:
		let/const { propertyA, propertyB, propertyC } = objectName;
		NOTE: will return an error if the dev did not use the correct propertyName
*/
const person = {
	givenName: "Jane",//if firstName - error will be returned since there is already a similar variable declared
	maidenName: "Dela", //if middleName - error will be returned since there is already a similar variable declared
	familyName: "Cruz" //if lastName - error will be returned since there is already a similar variable declared
};
// pre-es6/pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}!`);

// object destructuring
const { givenName, maidenName, familyName } = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);

// using object destructuring as a parameter of a function
function getFullName({ givenName, maidenName, familyName }){
	console.log(`${ givenName } ${ maidenName } ${ familyName }`);
};

getFullName(person);
/*
	Miniactivity
		create a pet object
			name
			trick
			treat
		create a function that will recieve a object destructuring as its parameter
			log in the console the message "<name>, <trick>!"
			log in the console the message "good <trick>!"
			log in the console the message "here is <treat> for you!"
		send the output in the google chat
*/

const pet = {
	name: "Steve",
	trick: "Swim",
	treat: "Fish"
}

function performTrick( { name, trick, treat } ){
	console.log(`${name}, ${trick}!`);
	console.log(`Good ${ trick }`);
	console.log(`Here is ${ treat } for you!`);
};

performTrick(pet);

// SECTION Arrow Function
	/*
		- compact alternative syntax to traditional functions
		- useful for code snippets where creating functions will not be reused in any other portions of the code

	*/

/*const hello = ()=>{
	console.log("Hello World");
};*/

// pre-es6/arrow function
/*function printFullName(firstName, middleInitial, lastName){
	console.log(`${firstName} ${middleInitial} ${lastName}`);
};*/

// arrow function in es6
/*
	let/const variableName = (parameterA, parameterB, parameterC) => {
		console.log();/ statements/expressions
	}
*/
let printFullName = (firstName, middleInitial, lastName) =>{
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName("Portgas", "D.", "Ace");
printFullName("Naruto", "U.", "Shippuden");

// arrow function with loops
const students = [ "John", "Jane", "Judy" ];
// pre-arrow function
students.forEach(function(student){
	console.log(`${student} is a student.`);
});

// SECTION - implicit return statement
/*
	- There are instances where we can omit the "return" statement;
	- This wokrs because even without the "return" statement, JS can implicitly adds it for the result of the function
*/
/*
	Miniactivity
		create an arrow with "add" as its name
			2 parameters, x y
			statement: using return keyword, add the x and y
		log in the console the function value once it is called with proper arguments
*/
// pre-arrow function
/*const add = ( x, y ) => { 
	return x + y;
};
console.log(add(99, 888));*/

// arrow function
const add = ( x, y ) => x + y;
let addVariable = add(99, 888);
console.log(addVariable);

// SECTION -Default Argument Value
/*
	the "name = 'User'" sets the default value for the function greet() once it is called without any arguments
*/
const greet = ( name = "User" ) =>{
	return `Good morning ${name}!`
};

console.log(greet("John"));
// this would return "Good Morning User" because the function is called without any arguments
// console.log(greet());

// SECTION - Class-based Object BluePrints
/*
	- Allows creation/instantiation of objects using classes as blueprints
*/
/*
Creating a class
	- "constructor" is a special method of a class for creating/initializing an object for that class
	- "this" keyword refers to the properties of an object created from the class; this allow us to reassign values for the properties inside the class
	SYNTAX:
		class ClassName{
			constructor (objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/
class Car{
	constructor (brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
};
// first instance
// using initializer and/or dot notation, create a car object using the Car class that we have created.
/*
	- "new" creates a new object with the given arguments as the values of its properties
	- no arguments provided will create an object without any values assigned to it, meaning the properties would return "undefined"
		SYNTAX:
			let/const variableName = new ClassName();
*/
// let myCar = new Car();
/*
	-creating with const keyword and assigning it a value of an object makes it so we cannot re-assign another data type i.e. array
	- it does not mean that it's properties cannot be changed/manipulated
*/
const myCar = new Car();
// console.log(myCar);

// Values for each property under the Car class
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

// second instance - using arguments
const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);